//
//  User.swift
//  App
//
//  Created by Hernan G. Gonzalez on 24/09/2018.
//

import FluentSQLite
import Vapor

typealias FacebookID = String

final class User: SQLiteUUIDModel {
    var id: UUID?
    var facebookID: FacebookID
    var name: String
}

/// Allows to be encoded to and decoded from HTTP messages.
extension User: Content { }

extension User: Parameter { }

extension User: Migration { }
