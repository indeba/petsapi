import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "Hello, world!" example
    router.get("ping") { req in
        return "alive"
    }

    // Example of configuring a controller
    let user = UserController()
    router.get("users", use: user.index)
    router.post("user", use: user.create)
    router.delete("user", User.parameter, use: user.delete)
}
