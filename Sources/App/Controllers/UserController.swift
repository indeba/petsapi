//
//  UserController.swift
//  App
//
//  Created by Hernan G. Gonzalez on 24/09/2018.
//

import Vapor

/// Controls basic CRUD operations on `Todo`s.
final class UserController {
    
    func index(_ req: Request) throws -> Future<[User]> {
        return User.query(on: req).all()
    }
    
    func create(_ req: Request) throws -> Future<User> {
        return try req.content.decode(User.self).flatMap { user in
            return user.save(on: req)
        }
    }
    
    /// Deletes a parameterized `Todo`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(User.self).flatMap { user in
            return user.delete(on: req)
        }.transform(to: .ok)
    }
}
